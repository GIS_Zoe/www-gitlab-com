---
title: "GitLab's Functional Group Updates - April 24th - May 5th" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Kirsten Abma
author_gitlab: kirstenabma
categories: company
description: "The Functional Groups at GitLab give an update on what they've been working on"
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/functional-group-updates/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

## Recordings

All of the updates are recorded using [Zoom](https://zoom.us) at the time of the call. All the recordings will be uploaded to our YouTube account and made public, with the exception of the Sales and Finance updates.

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### UX Team

[Presentation slides](https://docs.google.com/a/gitlab.com/presentation/d/163xL_FLQLsWD8Yv9GfkRdBHkqb_MqB-lnJBrihjQ-A0/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/NtDzyHnedoE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### CI Team

[Presentation slides](https://docs.google.com/presentation/d/1mmZFme78SURmonG-BKN1wYvN3zuw01I-jh6VMnsKu10/edit#slide=id.g1d9550ef42_0_42)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/1-KdQ--gq_Q" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Build Team

[Presentation slides](https://docs.google.com/presentation/d/1POef4yOW4MvAF43uPTPR1e9FN4Bx7rhihtwotf-VQDY/edit#slide=id.g153a2ed090_0_63)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/skXtSrBhSck" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Support Team

[Presentation slides](https://docs.google.com/presentation/d/1EizMPiTJFYm7R7Av6J7DguR_Crgo_t8pufLYKoGC5sU/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/hIm-idZFoeY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Infrastructure Team

[Presentation slides](https://docs.google.com/presentation/d/1SkoYYH3VCwHTdvW7rT3ZP0_XSahBx4f-7ON0-iqire0/pub?start=false&loop=false&delayms=3000&slide=id.g1ed640ff4e_0_2)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/sISWmcznwL8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Frontend Team

[Presentation slides](https://drive.google.com/a/gitlab.com/file/d/0B6SrD3vaBHUeNm4wUi1sY3VCNXc/view?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/YOqYNXGd1x4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Prometheus Team

[Presentation slides](https://docs.google.com/presentation/d/1e8z3t_rPqX5A9_6VYfGVGRIF1casz5SqBCFvsVyyNE4/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/9JDZ0NHKPUc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Channel Team

[Presentation slides](https://docs.google.com/presentation/d/191xTpGogIEtSGBdGeEthOm2ieZAkHitkJvc-dPXv1gM/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/IeKn0CgBzZI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----


Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
