---
layout: markdown_page
title: "Jira Software"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Summary
Jira Software is an issue tracker and agile project management application.
Portfolio for Jira Portfolio is a separate add-on that enables portoflio management in Jira Software.
Jira Service Desk is a separate application to enable IT and customer service capablilities.
Jira Core is a scaled down version of Jira Software that contains the general project managment capabilities without the software and agile-specific functionality of Jira Software.

Jira is available via 3 deploment models:

- Cloud: SaaS version of Jira hosted and managed by Atlassian
- Server: self-managed version that can be deployed on a single server
- Data Center: self-managed version that can be deployed to multiple servers for high availability.

### Website
[Jira Software](https://www.atlassian.com/software/jira)  
[Jira Portoflio](https://www.atlassian.com/software/jira/portfolio)  
[Jira Service Desk](https://www.atlassian.com/software/jira/service-desk)  
[Jira Core](https://www.atlassian.com/software/jira/core)  


### Integrations
GitLab has [Jira integration](https://about.gitlab.com/features/jira/) that allows Jira Software to be used as an issue tracker for the planning stage while using GitLab for the rest of the DevOps lifecycle: source code managment, CI/CD, and monitoring.

### Pricing
[Jira Software pricing](https://www.atlassian.com/software/jira/pricing)
- Cloud
  - Flat $10 per month for up to 10 users
  - $7 per user/month for 11-100 users
- Server: $3,600 one-time payment (50 users)
- Data Center: $12,000 per year (500 users)

[Portfolio for Jira pricing](https://www.atlassian.com/software/jira/portfolio/pricing)
- Cloud
  - Flat $10 per month for up to 10 users
  - $3.50 per user/month for 11-100 users
- Server: $9,900 one-time payment (500 users)
- Data Center: n/a

[Jira Service Desk pricing](https://www.atlassian.com/software/jira/service-desk/pricing)
- Cloud
  - Flat $10 per month for up to 3 agents
  - $20 per agent/month
- Sever: $13,200 one-time payment (50 agents)
- Data Ceneter: $60,000 per year (500 agents)

[Jira Core pricing](https://www.atlassian.com/software/jira/core/pricing)
- Cloud
  - Flat $10 per month for up to 10 users
  - $5 per user/month for 11-100 users
- Small teams: $10 One-time payment for up to 10 users
- Growing teams: $13,200 one-time payment (500 users)
