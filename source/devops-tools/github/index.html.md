---
layout: markdown_page
title: "GitHub"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Summary

### Website
[GitHub](https://www.github.com/)

### Comments/Anecdotes
- Feedback from customers is that GitHub Enterprise (GHE) has trouble scalling. It seems like anyone nearing 2k users starts to run into issues. [GitLab is enterprise class](/enterprise-class/) and scales to > 32K users.


### Pricing

