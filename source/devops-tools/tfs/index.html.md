---
layout: markdown_page
title: "Team Foundation Server (TFS)"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Team Foundation Server (TFS) Summary
Visual Studio Team Services ([VSTS](../vsts/)) is a hosted cloud offering, and Team Foundation Server (TFS), is an on-premises platform. Essentially, VSTS is a cloud, hosted version of TFS.Both offer functionality that cover multiple stages of the DevOps lifecycle including planning tools, source code managment (SCM), and CI/CD. 

As part of their SCM functionality, both platforms offer two methods of version control.

1. [Git](https://www.visualstudio.com/team-services/git/) (distributed) - each developer has a copy on their dev machine of the source repository including all branch and history information.

2. Team Foundation Version Control ([TFVC](https://www.visualstudio.com/team-services/tfvc/)), a centralized, client-server system - developers have only one version of each file on their dev machines. Historical data is maintained only on the server.

Microsoft recommends customers use Git for version control unless there is a specific need for centralized version control features. [https://docs.microsoft.com/en-us/vsts/tfvc/comparison-git-tfvc](https://docs.microsoft.com/en-us/vsts/tfvc/comparison-git-tfvc)

### Website
[Team Foundation Server](https://www.visualstudio.com/tfs/))


### Comments


### Pricing
[TFS Pricing](https://visualstudio.microsoft.com/team-services/tfs-pricing/)

A TFS license can be purchased as standalone product, but a TFS license (and CAL license) is also included when you buy a Visual Studio license / subscription. 

MS pushes Visual Studio subscriptions and refers customers who are only interested in a standalone TFS with a ‘classic purchasing’ model to license from a reseller.

Excluding CapEx and Windows operating system license, a standalone TFS license through a reseller in classic purchasing model is approximately $225 per year per instance.  The approximate Client Access License is approximately $320 per year.

