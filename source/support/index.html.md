---
layout: markdown_page
title: Support
description: "Visit our GitLab support page to find product documentation and to contact the support team."
---

## Search Our Documentation

Most of your questions can be answered by visiting [GitLab Documentation](https://docs.gitlab.com/), and using the powerful search function.

## Reaching Support


| Self-managed |
| --------------------- |
|  [GitLab Community Forum](https://forum.gitlab.com)<br/> I need help with a self-managed GitLab Core or Free edition server |
|  [GitLab.com Customer Support](https://support.gitlab.com)<br/> I need help with a paid license (Starter, Premium, Ultimate)|



| GitLab.com |
| --------------------- |
|  [GitLab Community Forum](https://forum.gitlab.com)<br/> I need help using GitLab.com |
|  [GitLab Support Forum Tracker](https://gitlab.com/gitlab-com/support-forum/issues)<br/> I need to report a general fault I encountered using GitLab.com |
|  [GitLab.com Account Support](https://support.gitlab.com)<br/> I need help accessing my GitLab.com account |
|  [GitLab.com Customer Support](https://support.gitlab.com)<br/> I need help with a paid subscription to GitLab.com (Bronze, Silver, Gold)|



Additional resources for getting help, reporting issues, requesting features, and so forth are listed on our [getting help page](/getting-help/).


### GitLab Support Offerings
#### Premium Support (Available for GitLab Premium, Ultimate, and Gold plans)

If your organization purchased a plan with **[Premium Support](/features/premium-support/)**, this includes:

   - **Regular requests**: 4 hour support response time.
   - **Emergency requests**: 30 minute response time, 24/7.
   - As part of receiving your license file, you should also have received a set of email addresses to use to reach Support for regular and emergency requests (separate addresses)
   - _How to submit regular requests:_ Please submit your regular support request through the [support web form](https://support.gitlab.com/) or via the regular support email in case the web form can't be reached for any reason. 
   - _How to submit EMERGENCY requests:_ Emergency requests must be submitted by email.
   - **Live upgrade assistance**: To schedule a time to have a Support Engineer be live with you via video-conference during an upgrade, please contact support at least 48 hours in advance.

If your organization would like to upgrade to a plan with Premium Support, you may purchase [online](https://customers.gitlab.com), email your account manager or email `renewals@gitlab.com`.

### Standard Support (Available for Starter, and Silver plans)

Subscribers with Standard Support receive next business day support via e-mail.

Please submit your support request through the [support web form](https://support.gitlab.com/). As part of receiving your license file, you should also have received an email address to reach Support in case the web form can't be reached for any reason.

### Support for GitHost

Subscribers to GitHost receive next business day support via e-mail.

Please submit your support request through the [support web form](https://support.gitlab.com/).

### Support for Community Edition

If you are seeking help with your GitLab Community Edition installation, please use the following resources:

* [GitLab Documentation](https://docs.gitlab.com): extensive Documentation is available regarding the Supported Configurations of GitLab.
* [GitLab Community Forum](https://forum.gitlab.com/): this is the best place to have a discussion about your chosen configuration and options.
* [Stack Overflow](http://stackoverflow.com/questions/tagged/gitlab): please search for similar issues before posting your own, there's a good chance somebody else had the same issue you have now and has resolved it.

Our [community advocates](/handbook/marketing/developer-relations/community-advocacy/) also spend time on the Community Forum and Stack Overflow to help where they can, and escalate issues as needed.


### Support for GitLab.com

Subscribers to [GitLab.com Plans](/pricing/#gitlab-com) receive next business day support via e-mail. Please submit your support request through the [support web form](https://support.gitlab.com/).

Even without a GitLab.com Bronze Support subscription, you can still receive the following support:

- If you have an issue accessing _your account_, please use the [support web form](https://support.gitlab.com/) so an admin can help you.
- For all other issues (downtime, functionality, etc.), please visit the [GitLab.com support tracker](https://gitlab.com/gitlab-com/support-forum/issues), and comment in an issue relevant to you or open a new one.
- Follow [GitLabStatus on Twitter](https://twitter.com/GitLabStatus) for status updates for the GitLab.com site.


## Contributing to GitLab

For more information on different ways to contribute, please visit the [Contributing](/contributing/) page.

## Further resources

Additional resources for getting help, reporting issues, requesting features, and so forth are listed on our [getting help page](/getting-help/).
