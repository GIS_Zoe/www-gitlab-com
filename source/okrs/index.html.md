---
layout: markdown_page
title: "Objectives and Key Results (OKRs)"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Quarterly OKRs

### [2018-Q3 (active)](/okrs/2018-q3/)
### [2018-Q2](/okrs/2018-q2/)
### [2018-Q1](/okrs/2018-q1/)
### [2017-Q4](/okrs/2017-q4/)
### [2017-Q3](/okrs/2017-q3/)

## What are OKRs?

OKRs are our quarterly goals to execute our [strategy](/strategy/). To make sure our goals are clearly defined and aligned throughout the organization. For more information see [Wikipedia](https://en.wikipedia.org/wiki/OKR) and [Google Drive](https://docs.google.com/presentation/d/1ZrI2bP-XKEWDWsT-FLq5piuIwl84w9cYH1tE3X5oSUY/edit) (GitLab internal). The OKRs are our quarterly goals.

## Format

Before the quarter:

`Owner: Objective as a sentence. Key result, key result, key result.`

During and after the quarter:

`Owner: Key Result as a sentence. Key result, key result, key result. => Outcome, outcome, outcome.`

- Each owner has a maximum of 3 objectives.
- Each objective has between 1 and 3 key results.
- Each key result has an outcome.
- Owner is the title of role that will own the result.
- We use four spaces to indent instead of tabs.
- The key result can link to an issue.
- The outcome can link to real time data about the current state.
- The three CEO objectives are level 3 headers to provide visual separation.

## Levels

We only list objectives prefaced with your role title.
We do OKRs up to the team level, we don't do [individual OKRs](https://hrblog.spotify.com/2016/08/15/our-beliefs/).
Although, an individual might have OKRs if they represent a unique function. 
For example, individual SDRs don't have OKRs, the SDR team does.
Legal is one person, but represents a unqiue function, so Legal has OKRs. 
Part of the individual performance review is the answer to: how much did this person contribute to the team objectives?
We have no more than [five layers in our team structure](/team/structure/).
Because we go no further than the manager level we end up with a maximum 4 layers of indentation on this page.
The match of one "nested" key result with the "parent" key result doesn't have to be perfect.
Every owner should have at most 3 objectives. To make counting easier always mention the owner with a trailing colon, like `Owner:`.
The advantage of this format is that the OKRs of the whole company will fit on three pages, making it much easier to have an overview.

## Updating

The key results are updated continually throughout the quarter when needed.
Everyone is welcome to a suggestion to improve them.
To update: make a merge request and assign it to the CEO.
If you're a [team member](/team/) or in the [core team](/core-team/) please post a link to the MR in the #okrs channel and at-mention the CEO.

At the top of the OKRs is a link to the state of the OKRs at the start of the quarter so people can see a diff.

Timeline of how we draft the OKRs:

1. CEO pushes top goals to this page: 5 weeks before the start of the quarter
1. Executive team pushes updates to this page: 4 weeks before the start of the quarter
1. Executive team 90 minute planning meeting: 3 weeks before the start of the quarter
1. Discuss with the board and the teams: 2 weeks before the start of the quarter
1. Executive team 'how to achieve' presentations: 1 week before the start of the quarter
1. Add Key Results to top of 1:1 agenda's: before the start of the quarter
1. Present OKRs at a functional group update: first week of the quarter
1. Present 'how to achieve' at a functional group update: during first three weeks of the quarter
1. Review previous quarter and next during board meeting: after the start of the quarter

## Scoring

It's important to score OKRs after the quarter ends to make sure we celebrate what went well, and learn from what didn't in order to set more effective goals and/or execute better next quarter.

1. Move the current OKRs on this page to an archive page _e.g._ [2017 Q3 OKRs](/okrs/2017-q3/)
1. Add in-line comments for each key result briefly summarizing how much was achieved _e.g._
  * "=> Done"
  * "=> 30% complete"
1. Add a section to the archived page entitled "Retrospective"
1. OKR owners should add a subsection for their role outlining...
  * GOOD
  * BAD
  * TRY
1. Promote the draft OKRs on this page to be the current OKRs

## Critical acclaim

Spontaneous chat messages from team members after introducing this format:

> As the worlds biggest OKR critic, This is such a step in the right direction :heart: 10 million thumbs up

> I like it too, especially the fact that it is in one page, and that it stops at the team level.

> I like: stopping at the team level, clear reporting structure that isn't weekly, limiting KRs to 9 per team vs 3 per team and 3 per each IC.

> I've been working on a satirical blog post called called "HOT NEW MANAGEMENT TREND ALERT: RJGs: Really Just Goals" and this is basically that. :wink: Most of these are currently just KPIs but I won't say that too loudly :wink: It also embodies my point from that OKR hit piece: "As team lead, it’s your job to know your team, to keep them accountable to you, and themselves, and to be accountable for your department to the greater company. Other departments shouldn’t care about how you measure internal success or work as a team, as long as the larger agreed upon KPIs are aligned and being met."

> I always felt like OKRs really force every person to limit freedom to prioritize and limit flexibility. These ones fix that!

## OKRs are stretch goals by default

OKRs should be ambitious but achievable. If you achieve less than 70% of your KR, it may have not been achievable. If you are regularly achieving 100% of your KRs, your goals may not be ambitious enough.

Some KRs will measure new approaches or processes in a quarter. When this happens, it can be difficult to determine what is ambitious and achievable because we lack experience with this kind of measurement. For these first iterations, we prefer to set goals that seem ambitious and expect a normal distribution of high, medium, and low achievement across teams with this KR.

## Hiring as an objective

The complete hiring plan is kept in the Hiring Forecast doc. Hiring is not an objective in-and-of-itself. However hiring critical members for a team can be considered a key result. This is because recruiting top technical talent in a competitive startup environment can consume a large proportion of management's time and those hires are modeled into our product development goals. Keep all hiring-related KR's in the 'Team' objective. Hiring goals cannot be stretch goals because we cannot open up more vacancies than are in the financial plan. So plan to hit 100% of your hiring goals.
