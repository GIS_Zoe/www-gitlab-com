---
layout: markdown_page
title: "Website Handbook"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Objectives

Serve the needs and interests of our key audiences:

1. Users of GitLab: software developers and IT operations practitioners.
2. Buyers of GitLab: IT management, software architects, development leads.
3. Users of and contributors to OSS on gitlab.com.

Generate demand for GitLab by:

1. Showcasing the benefits of the most important GitLab features and how they can save time and money.
2. Compare GitLab vs competing products.
3. Provide customer case studies which illustrate 1 and 2.

## Scope

When referring to the GitLab marketing site, `docs.gitlab.com` `gitlab.com` and
the `about.gitlab.com/handbook` are not included.

## Definitions

### Topics
A topic is an industry trend, theme, or technology related to GitLab and our customers. For example, DevOps, GDPR, Containers, etc. Topic pages on our website educate the reader about the topic and share GitLab’s point of view while providing additional links to resources related to that topic. These pages are intended to attract search traffic.

Topic pages should exist at the root level of the website without being nested inside of another directory. e.g. `/continuous-integration`

Examples of other companies who have topic pages:
- [https://www.redhat.com/en/topics/containers](https://www.redhat.com/en/topics/containers)
- [https://pivotal.io/containers](https://pivotal.io/containers)
- [https://pivotal.io/topics](https://www.redhat.com/en/topics/containers)

### Solutions
A solution is a combination of products and services that solve a business problem. For example, accelerating software delivery, enabling remote teams, ensuring compliance, etc. Solution pages on our website show the application of GitLab capabilities and services to address a business problem while providing additional links to resources related to that solution.

Solution pages should be nested inside of the solutions directory. e.g. `/solutions/continuous-integration`

Examples of other companies who have solutions pages:
- [https://www.redhat.com/en/challenges](https://www.redhat.com/en/challenges)

### Product section
The product section of our website has pages that describe what GitLab does and the value provided. The functionality of GitLab is ordered in a hierarchy with 4 levels: Stage, Categories, Capabilities, and Features. You can find details on the [Product Categories Handbook](/handbook/product/categories/)

- Stages relevant to users are listed on the [product overview page](/product) with details about the stage on the [stages page](https://gitlab.com/gitlab-com/www-gitlab-com/issues/2428).
- Categories relevant to users are listed on the [product overview page](/product).
- Capabilities are listed on the category page they belong to. Capabilities may also have their own landing page.
- Features are listed in many places on the website: on the features page, the capabilities page they belong to, the pricing page, comparison pages, and the ROI calculator.

Category pages should be nested inside of the product directory. e.g. `/product/continuous-integration`

Examples of companies who have product/features pages:
[https://mailchimp.com/features/](https://mailchimp.com/features/)
[https://www.groovehq.com/features](https://www.groovehq.com/features)

### Overlap

Similiar content can appear as a topic, solution, and in the product section with different emphases on each page. For example continuous integration:

- A topic page: `/continuous-integration` would talk about what CI is at a functional level.
- A solutions: `/solutions/continuous-integration`. would talk about why CI is important for businesses to adopt.
- A category page `/product/continuous-integration` would talk about the capabilities and features that are part of GitLab's CI functionality and the value it has.

## Ownership and responsibilities

The marketing site is an important part of our company requiring close coordination and collaboration across multiple teams. Below details which functional group is primarily responsible for which areas of the marketing site.

### Marketing Site Product Manager

[Luke Babb](/team/#lukebabb) is responsible for
scheduling tasks and allocating various team members to accomplish tasks.

- Track tasks on the [Website issue board](https://gitlab.com/gitlab-com/www-gitlab-com/boards/349137).

### Content Marketing

- Blog
- Press page
- Company pages
- Community pages
- Design input for the above

### Product Marketing

- Homepage
- Product
- Solutions
- Pricing
- Topics
- Customers and case study pages
- Sales enablement pages (e.g. comparison pages, ROI calculator)
- Design input for the above

### Growth Marketing

- CTAs (Calls to action): The growth team owns all buttons on the website including which pages get CTAS, the text used and where they link to. (Note: the website team still owns the design and styles of the buttons to ensure they are consistent and on brand.)
  - Any buttons tracked from the growth team should have a unique class used for tracking like `.free-trial-homepage-button`, rather than use a class that also distinguishes a buttons style e.g. `.btn`, `.cta-btn`, `.accent` etc. Using unique class names to track can also accompany classes that give the proper style. Classes on buttons we track should look something like this: `.btn.cta-btn.accent.free-trial-homepage-button`.
- Build and implement online growth strategy:
  - Search Engine Optimization
  - Paid search and social
  - Conversion Rate Optimization (CRO) and a/b and multivariate testing
      - CRO Process: Not all changes to pages need to be tested, only pages that we want to optimize for conversion activities. These pages/elements are often being tested
          - Homepage
          - Top navigation
          - Pricing page
          - Free trial
          - Contact sales
      - Please check [A/B test issue board](https://gitlab.com/gitlab-com/marketing/online-growth/boards?=&label_name[]=a%2Fb%20test) for pages you plan to update. The URL will be the first word or `HP` for the homepage.  If you have an update to any of the pages in the `Doing` column contact @lbanks. More details on CRO and testing in the [Online Growth Handbook section](/handbook/marketing/marketing-sales-development/online-marketing/)
-  Web analytics, tracking, and reporting
-  User journey optimization

### Design

*Design works with our partner agency, AtreNet, during this stage*
- Create sketches, wireframes, and mockups based on stakeholder input and user experience goals.
- Iterate on the design mockups until goals and objectives of the page are met.
- Produce a final Sketch file and artwork assets for the Frontend Development team to implement.
- Review front-end implementation (merge request) and provide feedback and guidance to improve the design.

### Frontend Development

*Design works with our partner agency, AtreNet, during this stage*

- In the spirit of 'everyone can contribute', pages and components of our website are built to be easily accessible and editable by non-technical members of the team (e.g. text & image updates).  Code quality is also important so that future developers or contributors can easily pick up on our workflow.
- Implement final approved design mockup.
- Pixel-push the design based on feedback from the Design team to match the final design mockup and vision for the page.

### All Product Managers

- Updating the *technical feature comparison tables* on
  `about.gitlab.com/comparison` and `about.gitlab.com/products` for the products
  they manage e.g GitLab column and competitor columns with list of feature
  names.

### Technical Writing

- Technical content on the `about.gitlab.com/installation` page (not the design
  and UX of this page which shoud be shared with the marketing site).
- Assist the Product Managers with the backlog of missing *technical feature
  comparison tables* on `about.gitlab.com/comparison` and
  `about.gitlab.com/products` e.g GitLab CI vs Jenkins.
  The Technical Writing team own the tasks from the backlog but will still have
  to ask the relevant Product Manger for content input as they know the
  feature's competitive landscape better than anyone.
- With regards to documentation the Technical Writing team is focusing on
  up to date and feature complete written documentation. No video content is
  planned for now.

## Updating the Marketing Website

### Minimum Viable Change

Use [MVCs](https://about.gitlab.com/handbook/values/#iteration) to update the website. A webpage only needs a title and a few lines of text to be valuable. Create new pages and add the minimimal ammount of content. You can add images and more content in iterative steps.

### Creating a new page
To create a new page you for follow these steps:
1. Create an issue in the [website repo](https://gitlab.com/gitlab-com/www-gitlab-com/issues) **Note**: Don't branch from other repos like the marketing repo.
2. Create an MR from the issue by clicking on the "Create Merge Request" button. This will create a new branch for you and link it to your issue and label the the MR as `WIP:`.
3. Click on the name of your branch after "Request to Merge" to open that branch in the repository file view.
4. Open the `source` folder. This is where webpages are stored.
5. Click on the directory where you want your webpage to be. For example, if you put a page in the `source` folder it will show up at the "root" level, if you create the new directory inside of another directory it will will appear at that path.
5. Click to add a `New directory` from the plus sign drop down.
6. Name the directory in all lowercase with dashes-between-words for what you want the path of your page to be. For example if you want to create a page at [about.gitlab.com/solutions/cloud-native](/solutions/cloud-native) then click on the `solutions` directory and inside the `solutions` directory create a new directory called `cloud-native`.
7. Click to add a `New file` from the plus sign drop down
8. Name the file `index.html.md`
9. Add this code to the top of the file

```
---
layout: markdown_page
title: ""
---
## Subheading

Here is your first paragraph replace this text.
```

10. Inside the quote add the title of your page. For example the title of my cloud native page would be "Building Cloud Native Applications With GitLab".
11. Using markdown you can add more content to the page. All you need is a title, subheading and a paragraph to get started.
12. Delete the `.gitkeep` file. This is a placeholder file from when you created the directory since git cannot track empty directories. A quick way to delete the file on the correct branch is to click on "edit" in the changes tab of your MR. This will open the file editor. click "cancel" and dismiss the popup that says "all changes will be lost". This will then place you in the file view for the `.gitkeep` file on your branch. Click the "delete" button to delete the file.
13. **ProTip**: Now that you no longer have a branch with no changes you can use the Web IDE to make further edits. (The Web IDE doesn't work if you have a branch with no changes. [Fix coming in 11.3](https://gitlab.com/gitlab-org/gitlab-ce/issues/48166)
14. **ProTip**: Add a link to the bottom of your page so people can continue reading related content. Popular choices would be `/product` , `/solutions`, `/pricing` or any pages related to your page.
15. If you need help you can Ping @williamchia or @jareko in the MR or on slack in the #website channel.  

## Updating the team page and org chart 
1. Both the [team page](/team/) and [org chart](/team/chart/) are updated based on [`team.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/team.yml)
2. Edit [`team.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/team.yml) and create an MR
3. The org chart is built automatically based on `slug` and `reports_to` lines. 
4. See the [team structure](https://about.gitlab.com/team/structure/#specialists-experts-and-mentors) page for more info on specialties, expertise, and mentorship availability to a listing.

### Updating an existing page
1. Click on the "edit" button at the bottom of the page.
1. Edit the page. Note: page content can be in markdown, `haml`, or possibly in a separate `.yml` file that populates fields in the `haml` file. The [hello bar](#editing-the-homepage-promo-banner-hello-bar) is an example of content in a `.yml` file.
1. If you need help you can Ping @gl-website in the MR or ping @website-team in the #website slack channel.  

### Updating the homepage promo banner (`hello-bar`)

- The homepage promo banner is edited at [`/source/includes/hello-bar.html.haml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/includes/hello-bar.html.haml).
- This banner includes an optional image (logo) to accompany the text.

### Adding content to resources
The [`/resources`](/resources/) section of the website contains downloadable files and links to helpful content. 

1. To add a resource, edit the [`resources.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/resources.yml) file.
2. To create a new entry for your resource, use this template and add it to the top of `resources.yml`:

```
- title:
  url:
  image: /images/resources/gitlab.png
  type: Pick one --> 'Blog post' 'One-pager' 'Report' 'Webcast' 'White paper'
  topics:
    - Cloud native
    - Code review
    - Continuous delivery
    - Continuous integration
    - DevOps
    - Git
    - GitLab
    - On-demand training
    - Public Sector
    - Security
    - Software development
  solutions:
    - Distributed teams
    - Accelerated delivery
    - Executive visibility
    - Project compliance
    - Security and quality
```

3. **Uploading a PDF**: If the resource you'd like to add is a PDF, it can be uploaded to [`/pdfs/resources/`](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/pdfs/resources/).

4. **Selecting an image**: Each resources has a thumbnail image based on their topic; select the most relevant image for the content. The current thumbnail images are shown below and can be found [here](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/images/resources).
![ALT](/images/resources/resource-page-thumbnails.png)

5. **Selecting a type**: All resource types are listed in the code snippet above; select one that best fits your content. If the resource does not fit the current types, please see [Requesting Website Updates](#requesting-website-updates) to submit a proposal for new resource type(s).

6. **Selecting topics & solutions**: The code snippet above provides all of the current topics and solutions; chose the topics and solutions that best apply to the content. Please note they are case sensitive and incorrect casing or spelling will result in the generation of new, unwanted, topics and/or solutions.

### Requesting Website Updates
If you'd like to propose new changes to the website and the update is more complicated that you can do on your own to either [create a new page](#creating-a-new-page) or [update and existing page](updating-an-existing-page) you can request help from the Website team.
1. Before requesting help, create the content that you want to go live. E.g. draft the exact words that you want updated.
1. To request help from the website team to update the site, create an issue in the [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/issues) project
1. Add the specific content (exact wording and images) in the issue description that you want to put live on the website. Note: If the the content is unclear, the issue will be assigned back to you to clarify the content before the website team will begin development work.
1. add the `Website` label
1. ping @gl-website in the MR or ping @website-team in the #website slack channel.
